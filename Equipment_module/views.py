from django.shortcuts import render
from django.views import View
from django.http import JsonResponse
from Main_module.mixins import TokenRequiredMixin, UserFullInfoContextMixin
from .models import Equipment, EquipCategory


class EquipmentView(TokenRequiredMixin, UserFullInfoContextMixin, View):
    template_name = 'Equipment_module/equipment_template.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(request)
        return render(request, self.template_name, context=context)

    def post(self, request):
        return render(request, self.template_name)


class EquipmentCatView(TokenRequiredMixin, UserFullInfoContextMixin, View):
    template_name = 'Equipment_module/equipment_items.html'

    def get(self, request, cat_name, *args, **kwargs):
        try:
            if cat_name != 'all':
                cat = EquipCategory.objects.get(name=cat_name)
                eqs = Equipment.objects.filter(category=cat)
            else:
                eqs = Equipment.objects.all()
        except EquipCategory.DoesNotExist:
            eqs = Equipment.objects.all()
        context = self.get_context_data(request)
        context['equips'] = eqs
        return render(request, self.template_name, context=context)

    def post(self, request):
        return render(request, self.template_name)


def get_equip_by_id(request, equip_id):
    try:
        myEquip = Equipment.objects.get(id=equip_id)
        return JsonResponse(myEquip.serialize(), status=201)
    except Equipment.DoesNotExist:
        return JsonResponse({}, status=404)
