from django.urls import path
from .views import EquipmentView, EquipmentCatView,  get_equip_by_id

urlpatterns = [
    path('', EquipmentView.as_view(), name="equipment"),
    path('<str:cat_name>', EquipmentCatView.as_view(), name="category"),
    path('item/<int:equip_id>', get_equip_by_id, name="equip_by_id"),
]
