from django.contrib import admin

from .models import Equipment, EquipCategory


class EquipItem(admin.ModelAdmin):
    list_display = ['title', 'price', 'status', 'credit', 'category']


admin.site.register(Equipment, EquipItem)
admin.site.register(EquipCategory)
