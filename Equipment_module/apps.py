from django.apps import AppConfig


class EquipmentModuleConfig(AppConfig):
    name = 'Equipment_module'
