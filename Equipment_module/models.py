from django.db import models
from tinymce.models import HTMLField
from PersonalAccount.settings import MEDIA_ROOT
import os

status = ((1, 'Показать'),
          (2, 'Скрыть'))


class EquipCategory(models.Model):
    name = models.CharField('name', max_length=50)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Категория Оборудования'
        verbose_name_plural = 'Категории Оборудования'


class Equipment(models.Model):
    image = models.FileField(upload_to='equipments/', verbose_name='Мини картинка', null=True)
    image2 = models.FileField(upload_to='equipments/', verbose_name='Большая картинка', null=True)
    title = models.CharField(max_length=250, null=False, verbose_name='Название')
    price = models.DecimalField(max_digits=10, decimal_places=2, default=0, verbose_name='Стоимость')
    credit = models.CharField(max_length=250, null=True, verbose_name='Текс в Кредит')
    status = models.IntegerField(verbose_name='Статус', choices=status)
    content = HTMLField(verbose_name='Описание')
    category = models.ForeignKey(EquipCategory, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.title + ' Цена:' + str(self.price) + ' (' + dict(status)[self.status] + ')'

    def serialize(self):
        return {
            'title': self.title,
            'content': self.content,
            'price': self.price,
            'credit': self.credit,
            'status': self.status,
            'image': self.image.url,
            'image2': self.image2.url,
        }

    class Meta:
        verbose_name = 'Оборудование'
        verbose_name_plural = 'Оборудования'
