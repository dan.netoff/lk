# Generated by Django 3.1.3 on 2021-01-19 12:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Equipment_module', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='equipment',
            name='status',
            field=models.CharField(choices=[(1, 'Показать'), (2, 'Скрыть')], max_length=10, verbose_name='Статус'),
        ),
    ]
