from django.shortcuts import render, redirect
from django.views import View
from django.http import JsonResponse
from Main_module.mixins import TokenRequiredMixin, UserFullInfoContextMixin
from Main_module import lk_api
import json
from django.http import HttpResponse
import requests


class PersonalServicesView(TokenRequiredMixin, UserFullInfoContextMixin, View):
    template_name = 'PersonalServices_module/personal_services_template.html'

    def get(self, request):
        context = self.get_context_data(request)
        resp, status_code = lk_api.getallservices(request.session['token'])

        # TODO HardWork!
        if status_code == 200:
            if len(resp['internet']) > 0:
                # print(resp['internet'][0]['serviceid'])
                my_dop_inet, stat = lk_api.getavaibledoptvpacks(request.session['token'], 0, resp['internet'][0]['serviceid'])
                # print(stat, my_dop_inet)
            if len(resp['tv']) > 0:
                my_dop_tv, stat = lk_api.getavaibledoptvpacks(request.session['token'], 1, resp['tv'][0]['serviceid'])
                # print(stat, my_dop_tv)
            context['ie'] = dict(resp['internet'][0])
            context['ie_candopip'] = resp['internet'][0]['candopip']
            print(json.dumps(resp['tv'], indent=4, sort_keys=True))
            print(json.dumps(my_dop_inet, indent=4, sort_keys=True))
            print(json.dumps(my_dop_tv, indent=4, sort_keys=True))
            context['tvs'] = (resp['tv'])
            context['ads'] = (resp['additionalServices'])
        else:
            return redirect('login')
        return render(request, self.template_name, context=context)

    def post(self, request):
        print(request.POST)
        res = lk_api.changemac(request.session['token'], request.POST['service_id'], request.POST['mac'])
        print(res)
        return redirect('my_services')


def userRequest(request):
    if request.method == "GET":

        data = request.POST

        # if not data.get('name') or not data.get('phone'):
        #     return HttpResponse(status=500)

        headers = {
            "Content-Type": "application/json"
        }

        url = request.META['HTTP_HOST']
        sub_dom = url.split('.')[0]
        ASSIGNED_BY = 237

        source = "4"
        if sub_dom == 'b2b':
            source = "5"

            if request.META['HTTP_REFERER'].find('0800') + 1:
                source = "10"
                ASSIGNED_BY = '28'

            if request.META['HTTP_REFERER'].find('crm') + 1:
                source = "11"
                ASSIGNED_BY = 237

        elif sub_dom == 'help':
            source = "6"

        elif sub_dom == 'cctv':
            source = "7"

        elif sub_dom == 'itv' or sub_dom == 'max' or sub_dom == 'loc' or sub_dom == '127':
            source = "4"

        future_lead = json.dumps(
            {
                "fields": {
                    "TITLE": f"TEST2 DAN2",
                    "NAME": 'from LK2',
                    "PHONE": [{"VALUE": '0507777777', "VALUE_TYPE": 'WORK'}],
                    "UTM_CAMPAIGN": data.get('utm_campaign') or '',
                    "UTM_CONTENT": data.get('utm_content') or '',
                    "UTM_MEDIUM": data.get('utm_medium') or '',
                    "UTM_SOURCE": data.get('utm_source') or '',
                    "UTM_TERM": data.get('utm_term') or '',
                    "ASSIGNED_BY_ID": '28',
                    "SOURCE_ID": source,
                    "UF_CRM_1596115393": '123'
                },
                "params": {"REGISTER_SONET_EVENT": "Y"}

            },
            ensure_ascii=False)

        # r = requests.post('https://crm.maxnet.ua/rest/15/lpxeb084eo284xnp/crm.lead.add.json',
        #                   data=future_lead.encode('utf-8'), headers=headers)

        # print(r.status_code)
        # if r.status_code == 200:
        #
        #     return HttpResponse(status=r.status_code)
        #
        # else:
        #     sendRequestToSIC(data)
        #     return HttpResponse(status=200)

    return HttpResponse(status=200)


def getchannelslist(request, packetid):
    channelslist, status_code = lk_api.getchannelslist(request.session['token'], packetid, 0)
    return JsonResponse({"response": channelslist}, status=status_code, safe=False)

