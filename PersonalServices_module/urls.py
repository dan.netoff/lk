from django.urls import path, include
from .views import PersonalServicesView, userRequest, getchannelslist

urlpatterns = [
   path('', PersonalServicesView.as_view(), name='my_services'),
   path('testcrm', userRequest),
   path('tvpacket/<int:packetid>', getchannelslist, name="channels_by_id"),
]
