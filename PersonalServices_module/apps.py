from django.apps import AppConfig


class PersonalservicesModuleConfig(AppConfig):
    name = 'PersonalServices_module'
