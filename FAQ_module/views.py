import requests
from django.shortcuts import render, redirect
from django.views import View
from Main_module.mixins import TokenRequiredMixin, UserFullInfoContextMixin


class FAQView(TokenRequiredMixin, UserFullInfoContextMixin, View):
    template_name = 'FAQ_module/faq_template.html'

    def get_context_data(self, request):
        context = super().get_context_data(request)
        r = requests.get('https://itv.maxnet.ua/api/faq/')
        if r.status_code == 200:
            context['faq_list'] = r.json()
        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(request)
        return render(request, self.template_name, context=context)

    def post(self, request, *args, **kwargs):
        return render(request, self.template_name)
