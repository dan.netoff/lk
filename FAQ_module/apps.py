from django.apps import AppConfig


class FaqModuleConfig(AppConfig):
    name = 'FAQ_module'
