from django.apps import AppConfig


class LoyaltyprogramsModuleConfig(AppConfig):
    name = 'LoyaltyPrograms_module'
