from django.urls import path
from .views import LoyaltyProgramsView

urlpatterns = [
    path('', LoyaltyProgramsView.as_view(), name="loyalty_program"),
]
