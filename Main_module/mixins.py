from django.shortcuts import redirect
from Main_module.lk_api import checktoken


class TokenRequiredMixin:
    def dispatch(self, request, *args, **kwargs):
        if request.method == 'POST':
            return self.post(request, *args, **kwargs)
        elif request.method == 'GET':
            if 'token' not in request.session:
                return redirect('login')
            elif not checktoken(request.session['token']):
                return redirect('login')
            return self.get(request, *args, **kwargs)


class UserFullInfoContextMixin:
    def get_context_data(self, request):
        context = {}
        if 'user' in request.session:
            context['user'] = request.session['user']
            context['token'] = request.session['token']
        return context
