from django.shortcuts import render, redirect
from django.views import View
from Main_module import lk_api
from Main_module.mixins import TokenRequiredMixin, UserFullInfoContextMixin


class IndexView(TokenRequiredMixin, UserFullInfoContextMixin, View):
    template_name = 'Main_module/main_module_template.html'

    def get(self, request):
        context = self.get_context_data(request)
        return render(request, self.template_name, context=context)

    def post(self, request):
        token, status_code = lk_api.authDefault(request.POST["login"], request.POST["password"])
        if status_code == 200:
            print(token)
            request.session['token'] = token
            user, status_code = lk_api.getuserinfofull(token)
            if status_code == 200:
                request.session['user'] = user
                # print(user)
            return redirect('index')
        else:
            return redirect('login')
