import requests
import json
import inspect

LK_API_URL = 'http://79.171.120.14:10201/lk/api/'


def authDefault(login, password):
    url = LK_API_URL + inspect.stack()[0][3]
    request_raw = '{' + f'"login": "{login}", "password": "{password}"' + '}'
    response = requests.post(url, data=request_raw)
    print(response.status_code, inspect.stack()[0][3])
    token = None
    if response.status_code == 200:
        res = json.loads(response.text)
        token = res['token']
        print(token)
    return token, response.status_code


def checktoken(token):
    url = LK_API_URL + inspect.stack()[0][3]
    request_headers = {"token": token}
    response = requests.post(url, headers=request_headers)
    print(response.status_code, inspect.stack()[0][3])
    return True if response.status_code == 200 else False


def getuserinfofull(token):
    url = LK_API_URL + inspect.stack()[0][3]
    request_headers = {"token": token}
    response = requests.post(url, headers=request_headers)
    print(response.status_code, inspect.stack()[0][3])
    res = None
    if response.status_code == 200:
        res = json.loads(response.text)
    return res, response.status_code


def getuserinfo(token):
    url = LK_API_URL + inspect.stack()[0][3]
    request_headers = {"token": token}
    response = requests.post(url, headers=request_headers)
    print(response.status_code, inspect.stack()[0][3])
    res = None
    if response.status_code == 200:
        res = json.loads(response.text)
    return res, response.status_code


def birthdatechange(token, date):
    url = LK_API_URL + inspect.stack()[0][3]
    request_headers = {"token": token}
    request_raw = '{' + f'"date": "{date}"' + '}'
    response = requests.post(url, headers=request_headers, data=request_raw)
    print(response.status_code, inspect.stack()[0][3])
    return response.status_code


def changeemail(token, email):
    url = LK_API_URL + inspect.stack()[0][3]
    request_headers = {"token": token}
    request_raw = '{' + f'"email": "{email}"' + '}'
    response = requests.post(url, headers=request_headers, data=request_raw)
    print(response.status_code, inspect.stack()[0][3])
    return response.status_code


def changephone(token, phone):
    url = LK_API_URL + inspect.stack()[0][3]
    request_headers = {"token": token}
    request_raw = '{' + f'"phone": "{phone}"' + '}'
    response = requests.post(url, headers=request_headers, data=request_raw)
    print(response.status_code, inspect.stack()[0][3])
    return response.status_code


def passwordchange(token, oldPassword, newPassword):
    url = LK_API_URL + inspect.stack()[0][3]
    request_headers = {"token": token}
    request_raw = '{' + f'"oldPassword": "{oldPassword}", "newPassword": "{newPassword}"' + '}'
    response = requests.post(url, headers=request_headers, data=request_raw)
    print(response.status_code, inspect.stack()[0][3])
    return response.status_code


def changemac(token, serviceid, mac):
    url = LK_API_URL + inspect.stack()[0][3]
    request_headers = {"token": token}
    request_raw = '{' + f'"serviceid": "{serviceid}", "mac": "{mac}"' + '}'
    response = requests.post(url, headers=request_headers, data=request_raw)
    print(response.status_code, inspect.stack()[0][3])
    return response.status_code


def getallservices(token):
    url = LK_API_URL + inspect.stack()[0][3]
    request_headers = {"token": token}
    response = requests.post(url, headers=request_headers)
    print(response.status_code, inspect.stack()[0][3])

    res = None
    if response.status_code == 200:
        res = json.loads(response.text)
        # print(res)
    return res, response.status_code


# API TEST
def getchannelslist(token, packetId, lang):
    url = LK_API_URL + inspect.stack()[0][3]
    request_headers = {"token": token}
    request_raw = '{' + f'"packetId": "{packetId}", "lang": "{lang}"' + '}'
    response = requests.post(url, headers=request_headers, data=request_raw)
    print(response.status_code, inspect.stack()[0][3])
    res = None
    if response.status_code == 200:
        res = json.loads(response.text)
    return res, response.status_code


def getpayhistory(token, year=None):
    url = LK_API_URL + inspect.stack()[0][3]
    request_headers = {"token": token}
    if year:
        request_raw = '{' + f'"year": "{year}"' + '}'
        response = requests.post(url, headers=request_headers, data=request_raw)
    else:
        response = requests.post(url, headers=request_headers)
    print(response.status_code, inspect.stack()[0][3])
    res = None
    if response.status_code == 200:
        res = json.loads(response.text)
    return res, response.status_code


def getdebithistory(token, year=None):
    url = LK_API_URL + inspect.stack()[0][3]
    request_headers = {"token": token}
    if year:
        request_raw = '{' + f'"year": "{year}"' + '}'
        response = requests.post(url, headers=request_headers, data=request_raw)
    else:
        response = requests.post(url, headers=request_headers)
    print(response.status_code, inspect.stack()[0][3])
    res = None
    if response.status_code == 200:
        res = json.loads(response.text)
    return res, response.status_code


def getchtphistory(token, year=None):
    url = LK_API_URL + inspect.stack()[0][3]
    request_headers = {"token": token}
    if year:
        request_raw = '{' + f'"year": "{year}"' + '}'
        response = requests.post(url, headers=request_headers, data=request_raw)
    else:
        response = requests.post(url, headers=request_headers)
    print(response.status_code, inspect.stack()[0][3])
    res = None
    if response.status_code == 200:
        res = json.loads(response.text)
    return res, response.status_code


def gettrafhistory(token, year=None, lang=0, ip=None):
    url = LK_API_URL + inspect.stack()[0][3]
    request_headers = {"token": token}
    request_raw = '{' + f'"lang": "{lang}"'
    if year:
        request_raw += ',' + f'"year": "{year}"'
    if ip:
        request_raw += ',' + f'"ip": "{ip}"'
    request_raw += '}'
    response = requests.post(url, headers=request_headers, data=request_raw)
    print(response.status_code, inspect.stack()[0][3])
    res = None
    if response.status_code == 200:
        res = json.loads(response.text)
    return res, response.status_code


def getservhistory(token, year=None):
    url = LK_API_URL + inspect.stack()[0][3]
    request_headers = {"token": token}
    if year:
        request_raw = '{' + f'"year": "{year}"' + '}'
        response = requests.post(url, headers=request_headers, data=request_raw)
    else:
        response = requests.post(url, headers=request_headers)
    print(response.status_code, inspect.stack()[0][3])
    res = None
    if response.status_code == 200:
        res = json.loads(response.text)
    return res, response.status_code


def smstypechange(token, sms_type):
    url = LK_API_URL + inspect.stack()[0][3]
    request_headers = {"token": token}
    request_raw = '{' + f'"sms_type": "{sms_type}"' + '}'
    response = requests.post(url, headers=request_headers, data=request_raw)
    print(response.status_code, inspect.stack()[0][3])
    return True if response.status_code == 200 else False


# API test
def getavaibledoptvpacks(token, servicetype, serviceid):
    url = LK_API_URL + inspect.stack()[0][3]
    request_headers = {"token": token}
    request_raw = '{' + f'"servicetype": "{servicetype}", "serviceid": "{serviceid}"' + '}'
    print(request_raw)
    response = requests.post(url, headers=request_headers, data=request_raw)
    print(response.status_code, inspect.stack()[0][3])
    res = None
    if response.status_code == 200:
        res = json.loads(response.text)
    return res, response.status_code


# API NEED TEST! NEED REMOVE
def adddoptvpack(token, servicetype, serviceid, packid):
    url = LK_API_URL + inspect.stack()[0][3]
    request_headers = {"token": token}
    request_raw = '{' + f'"servicetype": "{servicetype}", "serviceid": "{serviceid}", "packid": "{packid}"' + '}'
    print(request_raw)
    response = requests.post(url, headers=request_headers, data=request_raw)
    print(response.status_code, inspect.stack()[0][3])
    return response.status_code


def setcredit(token, days):
    url = LK_API_URL + inspect.stack()[0][3]
    request_headers = {"token": token}
    request_raw = '{' + f'"days": "{days}"' + '}'
    print(request_raw)
    response = requests.post(url, headers=request_headers, data=request_raw)
    print(response.status_code, inspect.stack()[0][3])
    return response.status_code


def getquestions(token, serviceid, lang=0):
    url = LK_API_URL + inspect.stack()[0][3]
    request_headers = {"token": token}
    request_raw = '{' + f'"serviceid": "{serviceid}", "lang": "{lang}"' + '}'
    print(request_raw)
    response = requests.post(url, headers=request_headers, data=request_raw)
    print(response.status_code, inspect.stack()[0][3])
    res = None
    if response.status_code == 200:
        res = json.loads(response.text)
        print(res)
    return res, response.status_code


def setrate(token, serviceid, questions, squestions, custom):
    url = LK_API_URL + inspect.stack()[0][3]
    request_headers = {"token": token}
    request_raw = '{' + f'"serviceid": "{serviceid}", "questions": {json.dumps(questions)}, "squestions":{json.dumps(squestions)}, "custom":"{custom}"' + '}'
    print(request_raw)
    response = requests.post(url, headers=request_headers, data=request_raw)
    print(response.status_code, inspect.stack()[0][3])
    return response.status_code


def adddopip(token, serviceid, mac):
    url = LK_API_URL + inspect.stack()[0][3]
    request_headers = {"token": token}
    request_raw = '{' + f'"serviceid": "{serviceid}", "mac": {mac}' + '}'
    print(request_raw)
    response = requests.post(url, headers=request_headers, data=request_raw)
    print(response.status_code, inspect.stack()[0][3])
    return response.status_code

