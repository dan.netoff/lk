'use strict';
$('.dogovor-input').mask('00000000000000000000');
const inputChecked = document.querySelector('.input-wrap');
const loginForm = document.querySelector('.login-form')
loginForm.addEventListener('change', (e) => {
    switch (e.target['id']) {
        case 'login_pass':
            inputChecked.innerHTML= `<input type="text" class="form-control input-place dogovor-input" placeholder="Номер договора" required>
                                     <input type="password" class="form-control input-place" placeholder="Пароль" required>`

            break;
        case 'phone':
            inputChecked.innerHTML= `<input type="text" class="form-control input-place phone-input" placeholder="+38 ХХХ ХХХ ХХ ХХ" required>
                                     `
            $('.phone-input').mask('+99 (999) 999-9999');
            break;
        case 'email':
            inputChecked.innerHTML= `<input type="email" class="form-control input-place" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$" placeholder="Введите E-mail" required>
                                     `
            break;

    }
})

//methods
document.forms['login-form'].addEventListener('submit', e => {
    e.preventDefault();
    let formData = new FormData(e.target);
    fetch('/login/', {
        method: 'POST',
        body: formData
    })
        .then(resp => {
            if (resp.status !== 201) {
                return Promise.reject(resp)
            }
            if (resp.status !== 555 /*тут номер ошибки не правильный пароль*/) {
                document.querySelector('.invalid-password').classList.add('invalid')
                return Promise.reject(resp)
            }

        })
        .catch(err => {
            console.log(err);
        })
})