const faq_field = document.querySelector('.faq_field'),
    category_title = faq_field.querySelector('.category_title'),
    faq_title = faq_field.querySelector('.faq_title'),
    faq_body = faq_field.querySelector('.faq_body'),
    categories_wrap = document.querySelector('.categories_wrap');

document.querySelectorAll('.q_list_link').forEach(item => {
    item.addEventListener('click', event => {
        event.preventDefault();
        category_title.innerHTML = item.closest('.card').querySelector('.cat_title').textContent;
        faq_title.innerHTML = item.textContent;
        faq_body.innerHTML = '';
        faq_body.insertAdjacentHTML('afterbegin', item.dataset.text);
        faq_field.scrollIntoView({
            behavior: 'smooth'
        });

    })
})

document.querySelector('.go_to_begin').addEventListener('click', event => {
    event.preventDefault();
    categories_wrap.scrollIntoView({
            behavior: 'smooth'
        });
})