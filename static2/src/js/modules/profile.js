'use strict';

function handleClick(cb) {
  console.log(cb.value);
  window.location.href = window.location.href + '?cb=' + cb.value
}


const loginForm = document.querySelectorAll('.profile-changer-form');
const bornForm = document.getElementById('born-form');
const emailForm = document.getElementById('email-form');
const phoneForm = document.getElementById('phone-form');
$('#change_phone').mask('+38(099)999-99-99');
$('#change_bdate').mask('00-00-0000');

loginForm.forEach( (item)=> {
    const dataEditButton = item.querySelector('[data-edit]')
    dataEditButton.addEventListener('click', (e)=> {
        switch (e.target.dataset.edit) {
        case 'born':
            bornForm.classList.add('active')
            bornForm.querySelector('input[aria-describedby="born_date_input"]').removeAttribute('readonly')
            // bornForm.querySelector('input[aria-describedby="born_date_input"]').value = ''
            break;
        case 'email':
            emailForm.classList.add('active')
            emailForm.querySelector('input[aria-describedby="email_input"]').removeAttribute('readonly')
            // emailForm.querySelector('input[aria-describedby="email_input"]').value = ''
            break;
        case 'phone':
            phoneForm.classList.add('active')
            phoneForm.querySelector('input[aria-describedby="phone_input"]').removeAttribute('readonly')
            // phoneForm.querySelector('input[aria-describedby="phone_input"]').value = ''
            break;
    }
    })
})

function closeProfileChanger (event) {
    let formClass = event.closest('.active');
    if (formClass.querySelector('input').value === '') {
        formClass.querySelector('input').value = formClass.querySelector('input').dataset.value
    }
    event.closest('.active').classList.remove('active');
}

