function readCookie(name) {
    let nameEQ = name + "=";
    let ca = document.cookie.split(';');
    for(let i=0;i < ca.length;i++) {
        let c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}


function pay_history(sel, url, token) {
let elem = document.getElementById("pay_tbody");
elem.innerHTML = '';
let fullsum = 0;

let url2 = 'https://lkdev.devgroup.pp.ua/history/getpayhistory'
let csrftoken = readCookie('csrftoken');

fetch(url2, {
    method: 'POST',
    headers: {
      'token': token,
        "X-CSRFToken":csrftoken,
        "year": sel.value
    }
  }).then(response => response.json())
  .then(data => {
    data.forEach(function(row) {
      elem.innerHTML += '<tr><td>' + row.date + '</td><td>' + row.source + '</td><td>' + row.payd_sum.replace('.', ',') + '</td></tr>'
      fullsum += parseFloat(row.payd_sum)
    })
    document.getElementById('pay_fullsum').innerHTML = '<b>' + fullsum.toFixed(2).replace('.', ',') + '</b>';
  })
}


function debit_history(sel, url, token) {
let elem = document.getElementById("debit_tbody");
elem.innerHTML = '';
let fullsum = 0;
let url2 = 'https://lkdev.devgroup.pp.ua/history/getdebithistory'
let csrftoken = readCookie('csrftoken');

fetch(url2, {
    method: 'POST',
    headers: {
      'token': token,
        "X-CSRFToken":csrftoken,
        "year": sel.value
    }
  }).then(response => response.json())
  .then(data => {
    data.forEach(function(row) {
      elem.innerHTML += '<tr><td>' + row.date + '</td><td>' + row.reason + '</td><td>' + row.sum.replace('.', ',') + '</td></tr>'
      fullsum += parseFloat(row.sum)
    })
    document.getElementById('debit_fullsum').innerHTML = '<b>' + fullsum.toFixed(2).replace('.', ',') + '</b>';
  })
}


function chtp_history(sel, url, token) {
let elem = document.getElementById("chtp_tbody");
elem.innerHTML = '';
let fullsum = 0;
let api_method = 'getchtphistory';

let url2 = 'https://lkdev.devgroup.pp.ua/history/getchtphistory'
let csrftoken = readCookie('csrftoken');

fetch(url2, {
    method: 'POST',
    headers: {
      'token': token,
        "X-CSRFToken":csrftoken,
        "year": sel.value
    }
  }).then(response => response.json())
  .then(data => {
    data.forEach(function(row) {
      elem.innerHTML += '<tr><td>' + row.date + '</td><td>' + row.type+ '</td><td>' + row.id+ '</td><td>' + row.src+ '</td><td>' + row.dst + '</td><td>' + row.sum.replace('.', ',') + '</td></tr>'
      fullsum += parseFloat(row.sum)
    })
    document.getElementById('chtp_fullsum').innerHTML = '<b>' + fullsum.toFixed(2).replace('.', ',') + '</b>';
  })
}

function traf_history(url, token, mon) {
let traf_ip = document.getElementById("traf_ip").value;
let traf_year = document.getElementById("traf_year").value;
let elem = document.getElementById("traf_tbody");
elem.innerHTML = '';
let api_method = 'gettrafhistory';
let my_mon = JSON.parse(mon);


let url2 = 'https://lkdev.devgroup.pp.ua/history/gettrafhistory'
let csrftoken = readCookie('csrftoken');

fetch(url2, {
    method: 'POST',
    headers: {
      'token': token,
        "X-CSRFToken":csrftoken,
        "year": traf_year,
        "ip": traf_ip,
        "lang": 1
    }
  }).then(response => response.json())
  .then(function (data){
      let trafs = data.traflist.pop();
    data.traflist.forEach(function(row) {
      elem.innerHTML += '<tr><td>' + my_mon[row.month] + '</td><td>' + row.in + '</td><td>' + row.out + '</td></tr>';
    })
     document.getElementById('traf_in').innerHTML = '<b>' + trafs.in + '</b>';
     document.getElementById('traf_out').innerHTML = '<b>' + trafs.out + '</b>';
  })
}


function serv_history(sel, url, token) {
let elem = document.getElementById("serv_tbody");
elem.innerHTML = '';
let url2 = 'https://lkdev.devgroup.pp.ua/history/getservhistory'

let csrftoken = readCookie('csrftoken');

fetch(url2, {
    method: 'POST',
    headers: {
      'token': token,
        "X-CSRFToken":csrftoken,
        "year": sel.value
    }
  }).then(response => response.json())
  .then(data => data.forEach(function(row) {
      if(row.type === '0'){
        row.type2 = 'Подключение'
      } else  {
        row.type2 = 'Сервис'
      }

      if(row.status === '1') {
        row.status2 = 'Выполнена'
      } else if (row.status === '2') {
        row.status2 = 'Открыта'
      } else if (row.status === '0') {
        row.status2 = 'Отмена'
      }
      elem.innerHTML += '<tr><td>' + row.id + '</td><td>' + row.date + '</td><td>' + row.time + '</td><td>' + row.type2 + '</td><td>' + row.cost + '</td><td>' + row.status2 + '</td></tr>'
    })
  )
}
