$('.modal-phone-input').mask('+99 (999) 999-9999');

$('#equipment-modal').on('show.bs.modal', function (event) {
    const equip_id = $(event.relatedTarget).data('equip_id')
    loadEquip(this, equip_id);

    function loadEquip(modal, equip_id) {
        const xhr = new XMLHttpRequest()
        const method = 'GET'
        const url = '/equipment/item/'+ equip_id

        xhr.responseType = 'json'
        xhr.open(method, url)
        xhr.onload = function () {
            const serverResponse = xhr.response
            const modal2 = $(modal)
            modal2.find('.content__title').text(serverResponse.title)
            modal2.find('.equipment-modal-content.text').html(serverResponse.content)
            modal2.find('.modal-img-wrap').html("<img loading='lazy' src='"+serverResponse.image2+"' alt=''>")
        }
        xhr.send()
    }
})
