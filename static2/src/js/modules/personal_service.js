$('#channels-modal').on('show.bs.modal', function (event) {
    const tv_id = $(event.relatedTarget).data('tv-id')
    loadChannels(this, tv_id);

    function loadChannels(modal, tv_id) {
        const xhr = new XMLHttpRequest()
        const method = 'GET'
        const url = '/my-services/tvpacket/'+ tv_id
        let final_list = '<div class="row">'

        xhr.responseType = 'json'
        xhr.open(method, url)
        xhr.onload = function () {
            const serverResponse = xhr.response.response
            const modal2 = $(modal)

            let i =0;
            for(i=0;i<serverResponse.length;i++){
                console.log(serverResponse[i])
                final_list = final_list + "<div class='col-lg-2 flex-column align-items-center'> <img src='" +
                    serverResponse[i].channelLogo +
                    "' alt=''><p class='text-center mt-2'>"+serverResponse[i].channelName+"</p></div>"
            }
            final_list = final_list + '</div>'
             modal2.find('.channels-wrap').html(final_list)

            //
            // modal2.find('.equipment-modal-content.text').html(serverResponse.content)
            // modal2.find('.modal-img-wrap').html("<img loading='lazy' src='"+serverResponse.image2+"' alt=''>")
        }
        xhr.send()
    }
})
