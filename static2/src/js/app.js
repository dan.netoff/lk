//notification
const notificationBtn = document.querySelector('.notify')
const notificationContent = document.querySelector('.notification-block')
const notificationCloseBtn = document.querySelector('.close-notify')
notificationBtn.addEventListener('click', (e) => {
    if (!notificationContent.classList.contains('active')) {
        notificationContent.classList.add('active')
    } else  notificationContent.classList.remove('active')
})
notificationCloseBtn.addEventListener('click', ()=> notificationContent.classList.remove('active') )


// collapse-channel-modal

$('#channelList_1').on('show.bs.collapse', function () {
    $('.channel-item-wrap').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 5,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            },
        ]

    });
})

$('#channelList_1').on('hidden.bs.collapse', function () {
  $('.channel-item-wrap').slick('unslick')
})



function clearInput(event) {
    event.closest('.delete-input-button').querySelector('input').value = ''
}