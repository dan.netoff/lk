from django.apps import AppConfig


class NewsModuleConfig(AppConfig):
    name = 'News_module'
