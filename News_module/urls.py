from django.urls import path
from .views import NewsView, news_article_view

urlpatterns = [
    path('', NewsView.as_view(), name='news_list'),
    path('<str:slug>/', news_article_view, name='news_article'),
]
