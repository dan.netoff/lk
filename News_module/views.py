from django.shortcuts import render
from django.views import View
from Main_module.mixins import TokenRequiredMixin, UserFullInfoContextMixin


class NewsView(View):
    template_name = 'News_module/news_list_template.html'

    def get(self, request, *args, **kwargs):
        # context = self.get_context_data(request)
        return render(request, self.template_name)

    def post(self, request):
        return render(request, self.template_name)


def news_article_view(request, slug):
    return render(request, 'News_module/news_article_template.html', {
        "test": "Акция «Включай возможности» для новых абонентов"
    })
