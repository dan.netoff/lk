from django.shortcuts import render
from django.views import View
from Main_module.mixins import TokenRequiredMixin, UserFullInfoContextMixin


class ContactsView(View):
    template_name = 'Contacts_module/contacts_template.html'

    def get(self, request, *args, **kwargs):
        # context = self.get_context_data(request)
        return render(request, self.template_name)

    def post(self, request):
        return render(request, self.template_name)
