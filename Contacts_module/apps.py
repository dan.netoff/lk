from django.apps import AppConfig


class ContactsModuleConfig(AppConfig):
    name = 'Contacts_module'
