from django.apps import AppConfig


class DocumentsModuleConfig(AppConfig):
    name = 'Documents_module'
