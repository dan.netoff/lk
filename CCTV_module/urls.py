from django.urls import path
from .views import CCTVView

urlpatterns = [
    path('', CCTVView.as_view(), name="cctv"),
]
