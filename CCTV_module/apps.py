from django.apps import AppConfig


class CctvModuleConfig(AppConfig):
    name = 'CCTV_module'
