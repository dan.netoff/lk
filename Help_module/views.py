from django.shortcuts import render, redirect
from django.views import View
from Main_module.mixins import TokenRequiredMixin, UserFullInfoContextMixin


class HelpView(TokenRequiredMixin, UserFullInfoContextMixin, View):
    template_name = 'Help_module/help_template.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(request)
        return render(request, self.template_name, context=context)

    def post(self, request):
        return render(request, self.template_name)
