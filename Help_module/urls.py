from django.urls import path
from .views import HelpView

urlpatterns = [
    path('', HelpView.as_view(), name='help'),
]
