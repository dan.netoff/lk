from django.apps import AppConfig


class HelpModuleConfig(AppConfig):
    name = 'Help_module'
