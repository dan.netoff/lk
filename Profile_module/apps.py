from django.apps import AppConfig


class ProfileModuleConfig(AppConfig):
    name = 'Profile_module'
