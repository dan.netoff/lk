from django.shortcuts import render, redirect
from django.views import View
from Main_module.mixins import TokenRequiredMixin, UserFullInfoContextMixin
from Main_module.lk_api import (
                                getuserinfofull,
                                passwordchange,
                                birthdatechange,
                                changeemail,
                                changephone,
                                smstypechange,
                                )


class ProfileView(TokenRequiredMixin, UserFullInfoContextMixin, View):
    template_name = 'Profile_module/profile_module.html'

    def get(self, request):

        if request.GET:
            print(request.GET)
            if 'born' in request.GET:
                birthdatechange(request.session['token'], request.GET['born'].replace('-', '.'))
                return redirect('profile')
            if 'email' in request.GET:
                changeemail(request.session['token'], request.GET['email'])
                return redirect('profile')
            if 'phone' in request.GET:
                changephone(request.session['token'], request.GET['phone'])
                return redirect('profile')
            if 'cb' in request.GET:
                cb = int(request.GET['cb'])
                st = int(request.session['user']['sms_type'])
                print(cb, st)
                if cb == 1:
                    if st == 0:
                        smstypechange(request.session['token'], 1)
                    elif st == 1:
                        smstypechange(request.session['token'], 0)
                    elif st == 2:
                        smstypechange(request.session['token'], 3)
                    elif st == 3:
                        smstypechange(request.session['token'], 2)
                elif cb == 2:
                    if st == 0:
                        smstypechange(request.session['token'], 2)
                    elif st == 1:
                        smstypechange(request.session['token'], 3)
                    elif st == 2:
                        smstypechange(request.session['token'], 0)
                    elif st == 3:
                        smstypechange(request.session['token'], 1)
                return redirect('profile')

        user, code = getuserinfofull(request.session['token'])
        if code == 200:
            request.session['user'] = user
        context = self.get_context_data(request)

        context['sms_info'] = 'checked' if request.session['user']['sms_type'] in ('1', '3') else ''
        context['sms_serv'] = 'checked' if request.session['user']['sms_type'] in ('2', '3') else ''
        return render(request, self.template_name, context=context)

    def post(self, request):
        return render(request, self.template_name)


class ProfilePasswordView(TokenRequiredMixin, UserFullInfoContextMixin, View):
    template_name = 'Profile_module/profile_password.html'

    def get(self, request):
        context = self.get_context_data(request)
        return render(request, self.template_name, context=context)

    def post(self, request):
        print(request.POST)
        context = self.get_context_data(request)
        if request.POST['newPassword'] == request.POST['newPassword2']:
            r_code = passwordchange(request.session['token'], request.POST['oldPassword'], request.POST['newPassword'])
            if r_code == 200:
                print('PASSWORD Change Success')
        return render(request, self.template_name, context=context)
