from django.urls import path
from .views import ProfileView, ProfilePasswordView

urlpatterns = [
    path('', ProfileView.as_view(), name="profile"),
    path('password', ProfilePasswordView.as_view(), name="profile_password"),
]
