from django.urls import path
from .views import HistoryView, getdebithistory2, gettrafhistory2, getchtphistory2, getpayhistory2, getservhistory2

urlpatterns = [
    path('', HistoryView.as_view(), name='history'),
    path('getdebithistory', getdebithistory2, name='getdebithistory'),
    path('gettrafhistory', gettrafhistory2, name='gettrafhistory'),
    path('getchtphistory', getchtphistory2, name='getchtphistory'),
    path('getpayhistory', getpayhistory2, name='getpayhistory'),
    path('getservhistory', getservhistory2, name='getservhistory'),


]
