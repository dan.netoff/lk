from django.http import JsonResponse
from django.shortcuts import render
from django.views import View
from Main_module.mixins import TokenRequiredMixin, UserFullInfoContextMixin
from Main_module.lk_api import (gettrafhistory,
                                getchtphistory,
                                getpayhistory,
                                getdebithistory,
                                getservhistory,
                                LK_API_URL,
                                )
from datetime import datetime
import json

month_name_ru = {
    1: 'Январь',
    2: 'Февраль',
    3: 'Март',
    4: 'Апрель',
    5: 'Май',
    6: 'Июнь',
    7: 'Июль',
    8: 'Август',
    9: 'Сентябрь',
    10: 'Октябрь',
    11: 'Ноябрь',
    12: 'Декабрь',
}

cb_years = list(range(datetime.now().year, datetime.now().year-4, -1))


class HistoryView(TokenRequiredMixin, UserFullInfoContextMixin, View):
    template_name = 'History_module/history_template.html'

    # TODO all .replace() move to APIs ?
    def get(self, request):
        context = self.get_context_data(request)

        trafs = gettrafhistory(request.session['token'], year=datetime.now().year)
        traf_sums = trafs[0]['traflist'].pop(-1)
        trafs_in, trafs_out = traf_sums['in'], traf_sums['out']
        for traf in trafs[0]['traflist']:
            traf['in'] = traf['in'].replace('.', ',')
            traf['out'] = traf['out'].replace('.', ',')
            traf['month'] = month_name_ru[int(traf['month'])]
        context['trafs_in'] = trafs_in
        context['trafs_out'] = trafs_out
        context['trafs'] = trafs[0]

        chtps = getchtphistory(request.session['token'], datetime.now().year)
        chtps_sum = 0
        for chtp in chtps[0]:
            chtps_sum += float(chtp['sum'].replace(',', '.'))
            # chtp['sum'] = chtp['sum'].replace('.', ',')
        context['chtps_sum'] = chtps_sum
        my_chtps = sorted(chtps[0], key=lambda i: (i['date'][3:5], i['date'][0:2]), reverse=True)
        context['chtps'] = my_chtps

        pays = getpayhistory(request.session['token'], datetime.now().year)
        pays_sum = 0
        for pay in pays[0]:
            pays_sum += float(pay['payd_sum'].replace(',', '.'))
            # pay['payd_sum'] = pay['payd_sum'].replace('.', ',')
        context['pays_sum'] = pays_sum
        my_pays = sorted(pays[0], key=lambda i: (i['date'][3:5], i['date'][0:2]), reverse=True)
        context['pays'] = my_pays

        debits = getdebithistory(request.session['token'], datetime.now().year)
        debits_sum = 0
        for deb in debits[0]:
            debits_sum += float(deb['sum'].replace(',', '.'))
            # deb['sum'] = deb['sum'].replace('.', ',')
        context['debits_sum'] = debits_sum
        my_debits = sorted(debits[0], key=lambda i: (i['date'][3:5], i['date'][0:2]), reverse=True)
        context['debits'] = my_debits

        context['token'] = request.session['token']
        context['url'] = LK_API_URL
        context['mon'] = json.dumps(month_name_ru)
        context['cb_years'] = cb_years

        return render(request, self.template_name, context=context)

    def post(self, request):
        return render(request, self.template_name)


def getdebithistory2(request):
    if request.method == 'POST':
        token = request.META['HTTP_TOKEN']
        year = request.META['HTTP_YEAR']
        trafs, status_code = getdebithistory(token, year=year)
        my_debits = sorted(trafs, key=lambda i: (i['date'][3:5], i['date'][0:2]), reverse=True)
        resp = JsonResponse(my_debits, status=200, safe=False)
        resp["Access-Control-Allow-Origin"] = "*"
        resp["Access-Control-Allow-Methods"] = "GET, POST"
        resp["Access-Control-Allow-Headers"] = "Content-Type"
        resp["Access-Control-Max-Age"] = "3600"
        return resp
    return JsonResponse({}, status=501)


def gettrafhistory2(request):
    if request.method == 'POST':
        token = request.META['HTTP_TOKEN']
        year = request.META['HTTP_YEAR']
        ip = request.META['HTTP_IP']
        trafs, status_code = gettrafhistory(token, year=year, ip=ip)
        resp = JsonResponse(trafs, status=200, safe=False)
        resp["Access-Control-Allow-Origin"] = "*"
        resp["Access-Control-Allow-Methods"] = "GET, POST"
        resp["Access-Control-Allow-Headers"] = "Content-Type"
        resp["Access-Control-Max-Age"] = "3600"
        return resp
    return JsonResponse({}, status=501)


def getchtphistory2(request):
    if request.method == 'POST':
        token = request.META['HTTP_TOKEN']
        year = request.META['HTTP_YEAR']
        trafs, status_code = getchtphistory(token, year=year)
        my_debits = sorted(trafs, key=lambda i: (i['date'][3:5], i['date'][0:2]), reverse=True)
        resp = JsonResponse(my_debits, status=200, safe=False)
        resp["Access-Control-Allow-Origin"] = "*"
        resp["Access-Control-Allow-Methods"] = "GET, POST"
        resp["Access-Control-Allow-Headers"] = "Content-Type"
        resp["Access-Control-Max-Age"] = "3600"
        return resp
    return JsonResponse({}, status=501)


def getservhistory2(request):
    if request.method == 'POST':
        token = request.META['HTTP_TOKEN']
        year = request.META['HTTP_YEAR']
        trafs, status_code = getservhistory(token, year=year)
        my_debits = sorted(trafs, key=lambda i: (i['date'][3:5], i['date'][0:2]), reverse=True)
        resp = JsonResponse(my_debits, status=200, safe=False)
        resp["Access-Control-Allow-Origin"] = "*"
        resp["Access-Control-Allow-Methods"] = "GET, POST"
        resp["Access-Control-Allow-Headers"] = "Content-Type"
        resp["Access-Control-Max-Age"] = "3600"
        return resp
    return JsonResponse({}, status=501)


def getpayhistory2(request):
    if request.method == 'POST':
        token = request.META['HTTP_TOKEN']
        year = request.META['HTTP_YEAR']
        trafs, status_code = getpayhistory(token, year=year)
        my_debits = sorted(trafs, key=lambda i: (i['date'][3:5], i['date'][0:2]), reverse=True)
        resp = JsonResponse(my_debits, status=200, safe=False)
        resp["Access-Control-Allow-Origin"] = "*"
        resp["Access-Control-Allow-Methods"] = "GET, POST"
        resp["Access-Control-Allow-Headers"] = "Content-Type"
        resp["Access-Control-Max-Age"] = "3600"
        return resp
    return JsonResponse({}, status=501)
