from django.apps import AppConfig


class HistoryModuleConfig(AppConfig):
    name = 'History_module'
