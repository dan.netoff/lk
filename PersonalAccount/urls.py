from django.contrib import admin
from django.urls import path, include
from django.conf.urls.i18n import i18n_patterns
from Auth_module.views import Logout
from PersonalAccount import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    path('admin/', admin.site.urls),

]

urlpatterns += i18n_patterns(
    path('', include('Main_module.urls')),
    path('login/', include('Auth_module.urls')),
    path('logout/', Logout.as_view(), name='logout'),
    path('services/', include('ServicesRequests_module.urls')),
    path('profile/', include('Profile_module.urls')),
    path('my-services/', include('PersonalServices_module.urls')),
    path('history/', include('History_module.urls')),
    path('equipment/', include('Equipment_module.urls')),
    path('loyalty/', include('LoyaltyPrograms_module.urls')),
    path('voip/', include('VoIP_module.urls')),
    path('documents/', include('Documents_module.urls')),
    path('faq/', include('FAQ_module.urls')),
    path('tv/', include('TV_module.urls')),
    path('cctv/', include('CCTV_module.urls')),
    path('help/', include('Help_module.urls')),
    path('payment/', include('Payment_module.urls')),
    path('news/', include('News_module.urls')),
    path('contacts/', include('Contacts_module.urls')),
    path('tinymce/', include('tinymce.urls')),

    prefix_default_language=False,
)

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
else:
    urlpatterns += staticfiles_urlpatterns()
# urlpatterns += staticfiles_urlpatterns()
# urlpatterns += static(settings.MEDIA_URL,
#                       document_root=settings.MEDIA_ROOT)
