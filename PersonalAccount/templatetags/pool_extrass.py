from django import template, urls
from django.urls import resolve
from typing import Dict, Any, Optional

register = template.Library()


@register.simple_tag(takes_context=True)
def check_active_url(context, *args):
    current_url = resolve(context.request.path_info).url_name
    res = ''
    for url in args:
        if url == current_url:
            res = 'active'
    return res


@register.simple_tag
def to_list(*args):
    return args


@register.filter()
def to_float(value):
    return float(int(value*100)/100)


@register.filter()
def to_str(value):
    return 'aaa'+str(value)


@register.filter()
def to_int(value):
    return int(value)


@register.filter(name='split')
def split(value, key):
    try:
        return value.split(key)
    except:
        return []


@register.simple_tag(takes_context=True)
def translate_url(context: Dict[str, Any], language: Optional[str]) -> str:
    """Get the absolute URL of the current page for the specified language.

    Usage:
        {% translate_url 'en' %}
    """
    # url = context['request'].build_absolute_uri()
    url = context['request'].get_full_path()
    site = context['request'].get_host()

    res = f'http://{site}{url}'

    link = urls.translate_url(res, language)
    # print(link)
    return link
