from django.apps import AppConfig


class TvNoduleConfig(AppConfig):
    name = 'TV_module'
