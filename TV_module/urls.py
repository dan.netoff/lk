from django.urls import path
from .views import TVView

urlpatterns = [
    path('', TVView.as_view(), name="tv"),
]
