from django.shortcuts import render, redirect
from Main_module.views import TokenRequiredMixin
from django.views import View


class Logout(TokenRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        request.session.pop('token', None)
        request.session.pop('user', None)
        return redirect('login')


def login_view(request):
    return render(request, 'Auth_module/auth_module_template.html',)


