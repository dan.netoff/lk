from django.apps import AppConfig


class AuthModuleConfig(AppConfig):
    name = 'Auth_module'
