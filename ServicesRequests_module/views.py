from django.shortcuts import render
from django.views import View
from django.http import JsonResponse
from Main_module.mixins import TokenRequiredMixin, UserFullInfoContextMixin
from Main_module.lk_api import getservhistory, LK_API_URL, getquestions
from Main_module import lk_api
from datetime import datetime
import json

cb_years = list(range(datetime.now().year, datetime.now().year-4, -1))


class ServicesListView(TokenRequiredMixin, UserFullInfoContextMixin, View):
    template_name = 'ServicesRequests_module/services_list_template.html'

    def get(self, request):
        context = self.get_context_data(request)
        quest = getquestions(request.session['token'], 243825)
        print(json.dumps(quest[0]['questions'], indent=4, ensure_ascii=False))
        servs = getservhistory(request.session['token'], year=datetime.now().year-1)
        for serv in servs[0]:
            serv['cost'] = serv['cost'].replace('.', ',')
            if serv['type'] == '0':
                serv['type2'] = 'Подключение'
            elif serv['type'] == '1':
                serv['type2'] = 'Сервис'
            if serv['status'] == '1':
                serv['status2'] = 'Выполнена'
            elif serv['status'] == '2':
                serv['status2'] = 'Открыта'
            elif serv['status'] == '0':
                serv['status2'] = 'Отмена'
        context['servs'] = servs[0]
        context['cb_years'] = cb_years
        context['url'] = LK_API_URL
        context['quiz'] = quest[0]['questions']

        return render(request, self.template_name, context=context)

    def post(self, request):
        return render(request, self.template_name)


class ServicesQuestionareView(TokenRequiredMixin, UserFullInfoContextMixin, View):
    template_name = 'ServicesRequests_module/services_questionnaire_template.html'

    def get(self, request, serv_id, serv_type):
        context = self.get_context_data(request)
        quest = getquestions(request.session['token'], serv_id)
        print(json.dumps(quest, indent=4, sort_keys=True))
        context['quiz'] = quest[0]['questions']
        context['quiz_num'] = 6
        if serv_type == '0':
            context['squiz'] = quest[0]['squestions']['6']['items']
            context['quiz_num'] = 7
        context['serv_id'] = serv_id
        context['serv_type'] = serv_type
        # TODO improve
        context['squer_id'] = 6
        return render(request, self.template_name, context=context)


class FeedbackView(TokenRequiredMixin, UserFullInfoContextMixin, View):
    template_name = 'ServicesRequests_module/feedback_template.html'

    def get(self, request):
        context = self.get_context_data(request)
        return render(request, self.template_name, context=context)

    def post(self, request):
        return render(request, self.template_name)


def setrate(request):
    if request.method == 'POST':
        data = json.loads(request.body.decode("utf-8"))
        status_code = lk_api.setrate(request.session['token'],
                                     data['serviceid'],
                                     data['questions'],
                                     data['squestions'],
                                     data['custom']
                                     )
        if status_code == 200:
            resp = JsonResponse({}, status=200, safe=False)
        else:
            resp = JsonResponse({}, status=202, safe=False)
        resp["Access-Control-Allow-Origin"] = "*"
        resp["Access-Control-Allow-Methods"] = "GET, POST"
        resp["Access-Control-Allow-Headers"] = "Content-Type"
        resp["Access-Control-Max-Age"] = "3600"
        return resp
    return JsonResponse({}, status=501)
