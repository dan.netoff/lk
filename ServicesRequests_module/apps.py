from django.apps import AppConfig


class ServicesrequestsModuleConfig(AppConfig):
    name = 'ServicesRequests_module'
