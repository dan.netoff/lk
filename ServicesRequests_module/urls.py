from django.urls import path
from .views import ServicesListView, ServicesQuestionareView, FeedbackView, setrate

urlpatterns = [
    path('', ServicesListView.as_view(), name="services_list"),
    path('feedback/<str:serv_id>/<str:serv_type>', ServicesQuestionareView.as_view(), name="services_questionnaire"),
    path('feedback/', FeedbackView.as_view(), name="feedback"),
    path('setrate/', setrate, name="setrate"),
]
