from django.apps import AppConfig


class PaymentModuleConfig(AppConfig):
    name = 'Payment_module'
