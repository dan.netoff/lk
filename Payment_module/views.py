from django.shortcuts import render
from django.views import View
from Main_module.mixins import TokenRequiredMixin, UserFullInfoContextMixin


class PaymentView(TokenRequiredMixin, UserFullInfoContextMixin, View):
    template_name = 'Payment_module/payment_template.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(request)
        return render(request, self.template_name, context=context)

    def post(self, request):
        return render(request, self.template_name)
