from django.urls import path
from .views import VoIPView

urlpatterns = [
    path('', VoIPView.as_view(), name="voip_view"),
]
