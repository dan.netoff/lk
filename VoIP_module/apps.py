from django.apps import AppConfig


class VoipModuleConfig(AppConfig):
    name = 'VoIP_module'
